import os
import logging
from logging.handlers import RotatingFileHandler
from .config import settings

_str_format = settings.Logging.Format

logging.basicConfig(
    level=logging.INFO,
    format=_str_format,
)

date_format = settings.Logging.DateFormat


log_file = "log/rates.log"

def get_file_handler(level = logging.INFO):
    if not os.path.exists('log'):
        os.mkdir('log')
    file_handler = RotatingFileHandler(log_file, maxBytes=1024000, backupCount=10)
    file_handler.setFormatter(logging.Formatter(_str_format, date_format))
    file_handler.setLevel(level)
    return file_handler

def get_stream_handler():
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter(_str_format, date_format))
    stream_handler.setLevel(logging.DEBUG)
    return stream_handler

def get_logger(name):
    logger = logging.getLogger(name)
    logger.addHandler(get_file_handler())
    logger.addHandler(get_stream_handler())
    return logger
    

