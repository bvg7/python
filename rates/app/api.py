from typing import Union
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.logger import logger
from .config import settings
from . import app_logger
from .routes import router

# Настройки логов
log = app_logger.get_logger("Rates")
logger.handlers = log.handlers

if settings.debug:
    log.info("Server configuration: %s, Host: %s:%s", settings.current_env, settings.host, settings.port)
    # log.info("Settings: %s", dict(settings))

# Настройки FastAPI
# docs_kwargs = {}  
# if settings.current_env == 'production':
#     docs_kwargs = dict(docs_url=None, redoc_url=None)  

app = FastAPI()#**docs_kwargs)

app.add_middleware(
    CORSMiddleware,
    **settings.cors
)

if settings.debug:
    log.info("Server user_middleware: %s", app.user_middleware)

app.include_router(router)
