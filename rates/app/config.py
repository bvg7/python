
import os
from dynaconf import Dynaconf

current_directory = os.path.abspath(os.path.dirname(__file__))

settings = Dynaconf(
    root_path=current_directory, # defining root_path
    envvar_prefix="FAPI",
    environments=True,
    load_dotenv=True,
    env_switcher="FAPI_ENV",
    dotenv_override=True,
    preload=[],    
    settings_files=['settings.yaml', '.secrets.yaml'],
    includes=[],
    merge_enabled=True,
)

# `envvar_prefix` = export envvars with `export DYNACONF_FOO=bar`.
# `settings_files` = Load these files in the order.
