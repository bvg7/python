from typing import Annotated
from fastapi import APIRouter, Query, HTTPException
from httpx import AsyncClient
import json
from .config import settings
from . import app_logger

# Настройки логов
log = app_logger.get_logger("Rates")

router = APIRouter()  
client = AsyncClient()

async def get_rates():
    url = f"{settings.rates_url}?apikey={settings.api_key}"
    try:
        # r = await client.get(url)
        # with open("rates.json", "wt", encoding="utf-8") as f:
        #     json.dump(r.json(), f)
        if settings.debug:
            with open("rates.json", "rt", encoding="utf-8") as f:
                d = json.load(f)
        else:
            r = await client.get(url)
            d = r.json()
    except Exception as e:  
        log.exception(e.__repr__())
        return None
    
    return d

### Routes ################################################################

@router.get("/", tags=["root"])
async def read_root():
    return "Server is runnung"

@router.get("/api/rates")
async def get_api_rates(
    rate_from: Annotated[str, Query(alias="from", min_length=3, max_length=3)],
    rate_to: Annotated[str, Query(alias="to", min_length=3, max_length=3)],
    value: Annotated[int, Query(gt=0, le=100)] = 1,
):
    rate_from = rate_from.upper()
    rate_to = rate_to.upper()
    rates = await get_rates()
    if settings.debug:
        log.info(rates)
    if not rates or not rates.get("data"):
        return 500
    rates = rates["data"]
    r1 = rates.get(rate_from)
    r2 = rates.get(rate_to)
    if not r1 or not r2:
        return {"msg": "Нет одной из указанных валют."}
        
    
    if r2 == 1:
        result = r1 * value
    elif r1 == r2:
        result = value
    else:
        result = (r1 / r2) * value

    return {"result": result}