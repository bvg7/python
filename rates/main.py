import uvicorn
from app import api
from app.config import settings

log_config = uvicorn.config.LOGGING_CONFIG
log_config["formatters"]["access"]["fmt"] = settings.Logging.Format
log_config["formatters"]["default"]["fmt"] = settings.Logging.Format

if __name__ == "__main__":
    if settings.env == 'production':
        uvicorn.run("app.api:app", host=settings.host, port=settings.port, workers=4, log_config=log_config)
    else:
        uvicorn.run("app.api:app", host=settings.host, port=settings.port, reload=True, log_config=log_config)        