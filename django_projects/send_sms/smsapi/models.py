from django.db import models

# Create your models here.


class Sending(models.Model):
    id = models.AutoField(primary_key=True) 
    name = models.CharField(verbose_name='Наименование', max_length=100, blank=False, null=False)
    dt_start = models.DateTimeField(verbose_name="Начало рассылки", null=False)
    text = models.TextField(verbose_name="Текст рассылки", blank=False, null=False)
    operator_code = models.CharField(verbose_name="Код мобильного оператора", max_length=10, blank=False, null=False)
    tag = models.CharField(verbose_name="Тег", max_length=20, blank=False, null=False)
    dt_stop = models.DateTimeField(verbose_name="Конец рассылки", null=False)

    class Meta:
        managed = True
        ordering = ('id', 'name',)
        verbose_name = "SMS Рассылка"
        verbose_name_plural = "SMS Рассылки"

    def __str__(self):
        return self.name


class Client(models.Model):
    id = models.AutoField(primary_key=True) 
    phone = models.CharField(verbose_name='Телефон', max_length=11, unique=True, blank=False, null=False)
    operator_code = models.CharField(verbose_name="Код мобильного оператора", max_length=10, blank=False, null=False)
    tag = models.CharField(verbose_name="Тег", max_length=20, blank=False, null=False)
    time_zone = models.CharField(verbose_name='Часовой пояс', max_length=20, blank=False, null=False)

    class Meta:
        managed = True
        ordering = ('id', 'phone',)
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return f"{self.phone} {self.operator_code} Time Zone: {self.time_zone} Tag: {self.tag}"


class Messages(models.Model):
    STATUS_CHOICES = [
        (0, "Не отпаравлено"),
        (1, "Отправлено"),
    ]
    id = models.AutoField(primary_key=True) 
    dt_start = models.DateTimeField(verbose_name="Дата и время отправки", null=False)
    status = models.PositiveSmallIntegerField(verbose_name="Статут", choices=STATUS_CHOICES, default=0, blank=False, null=False)
    id_sending = models.ForeignKey(Sending,verbose_name=Sending._meta.verbose_name, blank=True, null=True, on_delete=models.SET_NULL)
    id_client = models.ForeignKey(Client, verbose_name=Client._meta.verbose_name, blank=True, null=True, on_delete=models.SET_NULL)


    class Meta:
        managed = True
        ordering = ('id', 'dt_start',)
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f"{self.dt_start} {self.status}"
