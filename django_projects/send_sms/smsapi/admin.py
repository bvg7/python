from django.contrib import admin
from .models import Sending, Client, Messages

# Register your models here.

admin.site.register(Sending)

admin.site.register(Client)

admin.site.register(Messages)
