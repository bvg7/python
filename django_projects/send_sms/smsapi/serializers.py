from rest_framework import serializers
from .models import Client, Messages, Sending

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"

class MessagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messages
        fields = "__all__"

class SendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sending
        fields = "__all__"
