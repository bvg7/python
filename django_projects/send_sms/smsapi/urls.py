from django.urls import include, path, re_path

from smsapi import views

app_name = "smsapi"

urlpatterns = [
    # path('get_token', views.post_token, name="post_token"),
    # path('get_token/<str:ogrn>', views.get_token, name="get_token"),
    path('clients', views.ClientView.as_view(), name='clients'),
    path('clients/<str:id>', views.ClientView.as_view(), name='client'),
    path('sendings', views.SendingView.as_view(), name='sendings'),
    path('sending/<str:id>', views.SendingView.as_view(), name='sending'),
]
