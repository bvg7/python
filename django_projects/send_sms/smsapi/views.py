from datetime import datetime
from django.shortcuts import render
from django.conf import settings
from rest_framework import status, generics
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from django.contrib.auth.models import Permission
from django.http import JsonResponse
from .models import Client, Messages, Sending
from .serializers import ClientSerializer, MessagesSerializer, SendingSerializer

# Create your views here.


class ClientView(APIView):
    """
    Класс-представление для доступа к таблице Клиенты.
    """
    permission_classes = (IsAuthenticated, )
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def get_item(self, id):
        try: 
            return Client.objects.get(id = id)
        except:
            return None

    def get(self, request, id = None):
        if not id:
            serializer = self.serializer_class(self.queryset.all(), many = True)
            return Response({"status": "success", "data": serializer.data})

        client = self.get_item(id = id)
        if client == None:
            return Response({"status": "fail", "message": f"Client with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(client)
        return Response({"status": "success", "data": {"client": serializer.data}})

    def post(self, request):
        if settings.DEBUG:
            print(request.__dir__(), request.data, request.content_type, sep='\n')
        if not request.data:
            return Response({'detail': 'No data!'}, 404)
        data = request.data
        if settings.DEBUG:
            print(data)
        
        serializer = self.serializer_class(data = data)
        if serializer.is_valid():
            serializer.save()

            return Response({"status": "success", "data": {"client": serializer.data}}, status=status.HTTP_201_CREATED)
        else:
            return Response({"status": "fail", "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id):
        client = self.get_item(id)
        if client == None:
            return Response({"status": "fail", "message": f"Client with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(client, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.validated_data['updatedAt'] = datetime.now()
            serializer.save()
            return Response({"status": "success", "data": {"client": serializer.data}})
        return Response({"status": "fail", "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        client = self.get_item(id)
        if client == None:
            return Response({"status": "fail", "message": f"Client with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        client.delete()
        return Response({"status": "success", "message": f"Client with Id: {id} successfully deleted"}, status=status.HTTP_204_NO_CONTENT)


class SendingView(APIView):
    """
    Класс-представление для доступа к таблице Рассылки.
    """
    permission_classes = (IsAuthenticated, )
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    serializer_class = SendingSerializer
    queryset = Sending.objects.all()

    def get_item(self, id):
        try: 
            return self.queryset.get(id = id)
        except:
            return None

    def get(self, request, id = None):
        if not id:
            serializer = self.serializer_class(self.queryset.all(), many = True)
            return Response({"status": "success", "data": serializer.data})

        sending = self.get_item(id = id)
        if sending == None:
            return Response({"status": "fail", "message": f"Sending with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(sending)
        return Response({"status": "success", "data": {"sending": serializer.data}})

    def post(self, request):
        if settings.DEBUG:
            print(request.__dir__(), request.data, request.content_type, sep='\n')
        if not request.data:
            return Response({'detail': 'No data!'}, 404)
        data = request.data
        if settings.DEBUG:
            print(data)
        
        serializer = self.serializer_class(data = data)
        if serializer.is_valid():
            serializer.save()

            return Response({"status": "success", "data": {"sending": serializer.data}}, status=status.HTTP_201_CREATED)
        else:
            return Response({"status": "fail", "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id):
        sending = self.get_item(id)
        if sending == None:
            return Response({"status": "fail", "message": f"Sending with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(sending, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.validated_data['updatedAt'] = datetime.now()
            serializer.save()
            return Response({"status": "success", "data": {"sending": serializer.data}})
        return Response({"status": "fail", "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        sending = self.get_item(id)
        if sending == None:
            return Response({"status": "fail", "message": f"Sending with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)

        sending.delete()
        return Response({"status": "success", "message": f"Sending with Id: {id} successfully deleted"}, status=status.HTTP_204_NO_CONTENT)
